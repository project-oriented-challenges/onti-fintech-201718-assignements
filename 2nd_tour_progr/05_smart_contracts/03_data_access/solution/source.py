from web3 import Web3
from ethereum.utils import sha3, encode_int32, zpad, encode_hex, decode_hex

def get_balance_at_contract(c, a, bn):
    object_num = 1
    object_bytes = encode_int32(object_num)
    addr_bytes = zpad(decode_hex(a[2:]), 32)
    maddr = sha3(addr_bytes + object_bytes).hex()
    return web3.toInt(web3.eth.getStorageAt(c, int(maddr, 16), bn))

web3 = Web3(Web3.HTTPProvider('https://ropsten.infura.io/'))
eth = web3.eth

f = open('smart_contracts_4.txt', 'r')

c_address = web3.toChecksumAddress(f.readline().strip())
bn = int(f.readline())
balance = []
for i in f.readlines():
    address = web3.toChecksumAddress(i.strip())
    balance.append(get_balance_at_contract(c_address, address, bn))

print(sorted(balance)[4])
from web3 import Web3, HTTPProvider

web3 = Web3(Web3.HTTPProvider('https://ropsten.infura.io/'))

eth = web3.eth

# Считывание ABI метода 'checkin'
with open('scABI.txt', 'r') as f:
    scABI = f.read()
    
addr = input()
v = int(input())

scObj = web3.eth.contract(web3.toChecksumAddress(addr), abi=scABI)
print(scObj.functions.checkin(v).estimateGas())

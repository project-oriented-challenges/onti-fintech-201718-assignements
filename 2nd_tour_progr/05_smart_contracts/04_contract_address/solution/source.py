from sha3 import keccak_256
from py_ecc.secp256k1 import secp256k1
import rlp

def privToPub(priv):
    priv = int(priv, 16).to_bytes(32, 'big')
    res = secp256k1.privtopub(priv)
    x = res[0].to_bytes(32, 'big')
    y = res[1].to_bytes(32, 'big')
    return x+y

def pubToAddr(pub):
    return keccak_256(pub).hexdigest()[24:]

def privToAddr(priv):
    return pubToAddr(privToPub(priv))

with open('input.txt') as f:
    priv = f.readline().strip()

sender_addr = privToAddr(priv)
contract_addr = pubToAddr(rlp.encode([int(sender_addr, 16), 0]))
print(contract_addr)
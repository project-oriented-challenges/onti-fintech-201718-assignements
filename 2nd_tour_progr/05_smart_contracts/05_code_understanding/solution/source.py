from sha3 import keccak_256
from py_ecc.secp256k1 import ecdsa_raw_sign

def randomize(n):
    return (n*1)%2

def is_required(n, P):
    Mod = 65536
    return (P - (n * (((n + randomize(n)) % P)))) % Mod
    
def zpad(s, l):
    return '0' * (l-len(s)) + s
    
P = int(input())
priv = input()

i = 0
while is_required(i, P) != 0:
    i+=1
n = i

prefix = "\x19Ethereum Signed Message:\n".encode()
number = '32'.encode()
message = n.to_bytes(32, byteorder='big')
hash = prefix+number+message
h = keccak_256(hash).digest()

priv = int(priv, 16).to_bytes(32, 'big')
ans = ecdsa_raw_sign(h, priv)

r = zpad(hex(ans[1])[2:], 64)
s = zpad(hex(ans[2])[2:], 64)
v = zpad(str(ans[0]), 2)

print(r, s, v, sep = '\n')
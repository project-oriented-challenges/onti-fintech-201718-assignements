from py_ecc import secp256k1
from sha3 import keccak_256

def privToPub(priv):
    priv = int(priv, 16).to_bytes(32, 'big')
    res = secp256k1.privtopub(priv)
    x = res[0].to_bytes(32, 'big')
    y = res[1].to_bytes(32, 'big')
    return x+y

def pubToAddr(pub):
    return keccak_256(pub).hexdigest()[24:]

def privToAddr(priv):
    return pubToAddr(privToPub(priv))

def checkAddr(addr):
    hashAddr = bin(int(keccak_256(addr.encode()).hexdigest(), 16))[2:]
    hashAddr = '0' * (256 - len(hashAddr)) + hashAddr
    res = ''
    for i in range(len(addr)):
        if addr[i].isalpha() and hashAddr[4*i] == '1':
            res += str(i) + ' '
    return res

inp = open('input.txt', 'r')
out = open('output.txt', 'w')

for i in inp.readlines():
    i = i.strip()
    addr = privToAddr(i)
    out.write(addr + '\n')
    res = checkAddr(addr)
    out.write(res + '\n')

inp.close()
out.close()
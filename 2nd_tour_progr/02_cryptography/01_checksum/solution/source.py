def string_xor(s, code: str):
    result = 0
    while len(s) >= 4:
        result ^= int(s[:4].encode().hex(), 16)
        s = s[4:]
    if len(s) == 0:
        ans = hex((result ^ int(code, 16)))[2:]
        ans = '0' * (8-len(ans)) + ans
    else:
        result ^= int(code, 16)
        tempStr = (s.encode() + b'\x00'*(4 - len(s)%4)).hex()
        ans = hex(result ^ int(tempStr, 16))[2:]
        ans = '0' * (8 - len(ans)) + ans
        ans = ans[2*(len(s)%4):] + \
              ans[8-2*(len(s)%4):2*(len(s)%4)] + ans[:8-2*(len(s)%4)]

    return ans

code = input()
s = input()
print(string_xor(s, code))
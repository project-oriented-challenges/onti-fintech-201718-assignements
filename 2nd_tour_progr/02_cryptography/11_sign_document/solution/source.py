from py_ecc.secp256k1 import ecdsa_raw_sign
from sha3 import keccak_256

inp = open('input.txt', 'r')
out = open('output.txt', 'w')

n = 5

inp = inp.read()
inp = inp.split('\n')

for i in range(0, n*2, 2):
    priv = int(inp[i], 16).to_bytes(32, 'big')
    data = keccak_256(inp[i+1].encode()).digest()
    ans = ecdsa_raw_sign(data, priv)
    res = ''
    for j in range(3):
        if j > 0:
            res += '0' * (64 - len(hex(ans[j])[2:])) + hex(ans[j])[2:] + '\n'
        else:
            res += '0' * (2 - len(hex(ans[j])[2:])) + hex(ans[j])[2:] + '\n'
    out.write(res)
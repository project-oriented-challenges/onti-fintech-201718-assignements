from ethereum.transactions import Transaction
from ethereum import utils

res = ''
f = open('transactions.txt', 'r')
for i in f.readlines():
    in_tx = eval(i)
    t = Transaction(in_tx['nonce'], int(in_tx['gasPrice']), in_tx['gas'], in_tx['to'],
        int(in_tx['value']), in_tx['input'][2:], int(in_tx['v'], 16),
        int(in_tx['r'], 16), int(in_tx['s'], 16))
    try:
        sender = utils.encode_hex(t.sender)
        if sender == in_tx['from'][2:]:
            res += '1'
        else:
            res += '0'
    except:
        res += '0'
print(int(res[::-1], 2))
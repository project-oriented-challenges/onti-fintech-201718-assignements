from py_ecc import secp256k1
from sha3 import keccak_256
from py_ecc.secp256k1.secp256k1 import add

# return public key in byteArray
def privToPub(priv):
    priv = int(priv, 16).to_bytes(32, 'big')
    res = secp256k1.privtopub(priv)
    x = res[0].to_bytes(32, 'big')
    y = res[1].to_bytes(32, 'big')
    return x+y

# return address in hex
def pubToAddr(pub):
    return keccak_256(pub).hexdigest()[24:]

# param s: 64 length byteArray
def prepareToAdd(s):
    return (int.from_bytes(s[:32], 'big'), int.from_bytes(s[32:], 'big'))

# return public key in byteArray
def vanityAdd(x, y):
    res = add(x, y)
    return res[0].to_bytes(32, 'big') + res[1].to_bytes(32, 'big')

pub = input()
key = input()

pub = int(pub, 16).to_bytes(64, 'big')
pub = prepareToAdd(pub)

for i in range(2**256):
    i = hex(i)[2:]
    privAdd = privToPub(i)
    privAdd = prepareToAdd(privAdd)
    a = pubToAddr(vanityAdd(pub, privAdd))
    if a[:4] == key:
        print('0' * (64-len(i)) + i)
        break
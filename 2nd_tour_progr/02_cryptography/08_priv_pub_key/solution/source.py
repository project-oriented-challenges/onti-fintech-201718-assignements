from py_ecc import secp256k1

def privToPub(priv):
    priv = int(priv, 16).to_bytes(32, 'big')
    res = secp256k1.privtopub(priv)
    a = res[0].to_bytes(32, 'big')
    b = res[1].to_bytes(32, 'big')
    pub = int.from_bytes(a + b, 'big')
    return '0' * (128-len(hex(pub)[2:])) + hex(pub)[2:]

inp = open('input.txt', 'r')
out = open('output.txt', 'w')

for i in inp.readlines():
    i = i.strip()
    out.write(privToPub(i) + '\n')

inp.close()
out.close()
from _pysha3 import keccak_256
import datetime

def increaseTime(curr_datetime: datetime):
    if curr_datetime.hour == 23:
        curr_datetime += datetime.timedelta(hours=1)
    else:
        curr_datetime += datetime.timedelta(hours=23)
    return curr_datetime

year = int(input())
code = input()
year, month, day = year-1, 12, 31
ans = ""
dfTime = datetime.datetime(year, month, day, 23, 0, 0)

while ans != code:
    dfTime = increaseTime(dfTime)
    seconds = dfTime.timestamp()
    tempTime = int.to_bytes(int(seconds), 32, byteorder='big')
    ans = keccak_256(tempTime).hexdigest()
if dfTime.hour == 23:
    dfTime = increaseTime(dfTime)

print('{num:02d}'.format(num=dfTime.day),'{num:02d}'.format(num=dfTime.month), sep='.')
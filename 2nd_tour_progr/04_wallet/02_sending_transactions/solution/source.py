from web3 import Web3, HTTPProvider

web3 = Web3(HTTPProvider('http://localhost:48002'))
eth = web3.eth
personal = web3.personal

accounts = []
f = open('accounts.txt', 'r')
for i in f:
    accounts.append(i.strip())
f.close()

with open('input.txt') as f:
    data1 = '0x' + f.readline().strip()
    data2 = '0x' + f.readline().strip()

personal.unlockAccount(accounts[0])

transaction1 = eth.sendTransaction({'from': accounts[0], 'to': accounts[1],
                                    'value': 1, 'data': data1})
transaction2 = eth.sendTransaction({'from': accounts[0], 'to': accounts[1], 
                                    'value': 1, 'data': data2})

print(abs(eth.getTransaction(transaction2)['gas'] - eth.getTransaction(transaction1)['gas']))
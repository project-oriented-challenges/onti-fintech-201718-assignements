def get_max(addr: list, block_n: int):
    maxeth = -1
    for i in addr:
        if eth.getBalance(i, block_n) > maxeth:
            maxeth = eth.getBalance(i, block_n)
    return maxeth

def convert(unit: str, bal):
    if unit == 'wei':
        return bal
    if unit == 'kwei' or unit == 'ada':
        return int(bal / 10**3)
    if unit == 'mwei' or unit == 'babbage':
        return int(bal / 10**6)
    if unit == 'gwei' or unit == 'shannon':
        return int(bal / 10**9)
    if unit == 'szabo':
        return int(bal / 10**12)
    if unit == 'finney':
        return int(bal / 10**15)
    return int(bal / 10**18)

from web3 import Web3

web3 = Web3(Web3.HTTPProvider('https://ropsten.infura.io/'))
eth = web3.eth

n = 20
addr = []
f = open('input.txt', 'r')
block_n = int(f.readline().strip())
unit = f.readline().strip()
for i in f.readlines():
    addr.append(web3.toChecksumAddress(i.strip()))

ans = convert(unit, get_max(addr, block_n))
print(ans)
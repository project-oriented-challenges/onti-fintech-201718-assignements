from ethereum import db
from ethereum import trie
from ethereum import utils
import rlp

def countExtensionNodesWithLeaves(state, node, ext_size, flag, leaves_num):
    if state._get_node_type(node) == trie.NODE_TYPE_LEAF and flag:
        leaves_num += 1
    if state._get_node_type(node) == trie.NODE_TYPE_EXTENSION:
        temp = state._decode_to_node(node[1])
        for i in state._decode_to_node(temp):
            if i != b'':
                ext_size += 1
        flag = True
        ext_size, leaves_num = count_extension_nodes(state, state._decode_to_node(temp),
            ext_size, flag, leaves_num)
    elif state._get_node_type(node) == trie.NODE_TYPE_BRANCH:
        for i in node:
            if i != b'':
                temp = state._decode_to_node(i)
                ext_size, leaves_num = count_extension_nodes(state, temp, 
                    ext_size, flag, leaves_num)
    return ext_size, leaves_num

res = []

for i in range(3):
    n = int(input())

    state = trie.Trie(db.EphemDB(), trie.BLANK_ROOT)
    for i in range(0, n):
        state.update(rlp.encode(i), b'')

    res.append(countExtensionNodesWithLeaves(state, state.root_node, 0, False, 0))
    
for i in res:
    print(*i)
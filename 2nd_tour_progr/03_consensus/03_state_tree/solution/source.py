from ethereum import transactions
from ethereum import utils
import rlp

# Возвращает состояние балансов на момент обнаружения некорректной транзакции
def get_state(f):
    balances = dict()
    n = int(f.readline())
    for i in range(n):
        t_balances = balances.copy()
        tx_num, miner = f.readline().strip().split()
        for j in range(int(tx_num)):
            tx_hash = f.readline().strip()
            tx = (rlp.decode(utils.decode_hex(tx_hash), transactions.Transaction)).to_dict()
            sender, to, value = tx['sender'][2:], tx['to'][2:], int(tx['value'])
            if t_balances.get(sender, 0) - value < 0:
                return balances
            t_balances[sender] -= value
            t_balances[to] = t_balances.get(to, 0) + value
        balances = t_balances.copy()
        balances[miner] = balances.get(miner, 0) + 5*10**18
    return balances
    
f = open('input.txt', 'r')
balances = get_state(f)

sorted_balances = sorted(list(balances.values()), reverse=True)

res = 0
n = len(sorted_balances)
for i in range(min(n, 5)):
    res += sorted_balances[i]
    
print(res)
import hashlib, binascii

def hash2(a, b):
    a1 = binascii.a2b_hex(a)[::-1]
    b1 = binascii.a2b_hex(b)[::-1]
    h = hashlib.sha256(hashlib.sha256(a1+b1).digest()).digest()
    return h[::-1].hex()

def merkle(hashList):
    if len(hashList) == 1:
        return hashList[0]
    newHashList = []
    for i in range(0, len(hashList)-1, 2):
        newHashList.append(hash2(hashList[i], hashList[i+1]))
    if len(hashList) % 2 == 1:
        newHashList.append(hash2(hashList[-1], hashList[-1]))
    return merkle(newHashList)

a = []

n = int(input())

for i in range(n):
    a.append(input())

print(merkle(a))
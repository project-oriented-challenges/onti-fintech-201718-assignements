from ethereum.utils import encode_hex, decode_hex, big_endian_to_int
from ethereum.block import BlockHeader
import pyethash

with open('ethash.txt', 'r') as f:
    desc = eval(f.read())
    
blockHeader = BlockHeader(decode_hex(desc['parentHash'][2:]), 
    decode_hex(desc['sha3Uncles'][2:]),
    desc['miner'][2:], decode_hex(desc['stateRoot'][2:]), 
    decode_hex(desc['transactionsRoot'][2:]), 
    decode_hex(desc['receiptsRoot'][2:]), desc['logsBloom'], 
    desc['difficulty'], desc['number'], 
    desc['gasLimit'], desc['gasUsed'], desc['timestamp'], 
    decode_hex(desc['extraData'][2:]),
    nonce=desc['nonce'])
    
b_number = blockHeader.number
b_hash = blockHeader.mining_hash
b_nonce = decode_hex(blockHeader.nonce[2:])
cache = pyethash.mkcache_bytes(b_number)

ans = pyethash.hashimoto_light(b_number, cache, b_hash, 
                               big_endian_to_int(b_nonce))[b'mix digest']

print(encode_hex(ans))
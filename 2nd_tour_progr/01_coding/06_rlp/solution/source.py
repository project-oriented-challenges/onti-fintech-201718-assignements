def RLPDecode(s: str):
    result = ''
    temp = s[:2]
    if 0 <= int(temp, 16) <= 191:
        result += RLPDecodeString(s)
    else:
        result += RLPDecodeArray(s)
    return result

def RLPDecodeString(s: str):
    result = ''
    temp = s[:2]
    result += temp
    if 128 <= int(temp, 16) <= 183:
        for i in range(len(s)-2, 1, -2):
            result += s[i] + s[i+1]
    elif 184 <= int(temp, 16) <= 191:
        tsize = int(temp, 16) - 183
        for i in range(1, int(temp, 16) - 183 + 1):
            temp = s[2*i:2*i+2]
            result += temp
        for i in range(len(s) - 2, 1+tsize*2, -2):
            result += s[i] + s[i+1]
    return result

def RLPDecodeArray(s: str):
    result = ''
    elements = []
    temp = s[:2]
    result += temp
    if 192 <= int(temp, 16) <= 247:
        i = 2
        while i < len(s):
            temp = s[i:i+2]
            if 0 <= int(temp, 16) <= 191:
                tempPair = RLPDepodeStringFromArray(s[i:])
                elements.append(tempPair[0])
                i += tempPair[1]
            elif int(temp, 16) <= 255:
                tempPair = RLPDecodeArrayFromArray(s[i:])
                elements.append(tempPair[0])
                i += tempPair[1]
    elif int(temp, 16) <= 255:
        tsize = int(temp, 16) - 247
        for i in range(1, int(temp, 16) - 247 + 1):
            temp = s[2*i:2*i+2]
            result += temp
        i = tsize*2 + 2
        while i < len(s):
            temp = s[i:i+2]
            if 0 <= int(temp, 16) <= 191:
                tempPair = RLPDepodeStringFromArray(s[i:])
                elements.append(tempPair[0])
                i += tempPair[1]
            elif int(temp, 16) <= 255:
                tempPair = RLPDecodeArrayFromArray(s[i:])
                elements.append(tempPair[0])
                i += tempPair[1]
    for i in range(len(elements)-1, -1, -1):
        result += elements[i]
    return result

# Находит закодированную строку
def RLPDepodeStringFromArray(s: str):
    result = ''
    tempSize = 0
    temp = s[:2]
    if 0 <= int(temp, 16) <= 127:
        result += temp
        tempSize = 2
    elif int(temp, 16) <= 183:
        size = int(temp, 16) - 128
        result += s[:2+size*2]
        tempSize = 2+size*2
    elif int(temp, 16) <= 191:
        tsize = int(temp, 16) - 183
        temp = s[2:2*tsize+2]
        size = int(temp, 16)
        result += s[:size*2+tsize*2+2]
        tempSize = size*2+tsize*2+2
    return (result, tempSize)

# Находит закодированный массив
def RLPDecodeArrayFromArray(s: str):
    result = ''
    tempSize = 0
    temp = s[:2]
    if 192 <= int(temp, 16) <= 247:
        size = int(temp, 16) - 192
        result += s[:2+size*2]
        tempSize = 2+size*2
    elif int(temp, 16) <= 255:
        tsize = int(temp, 16) - 247
        temp = s[2:2*tsize+2]
        size = int(temp, 16)
        result += s[:size*2+tsize*2+2]
        tempSize = size*2+tsize*2+2
    return (result, tempSize)

f = open('input.txt', 'r')
out = open('output.txt', 'w')
for i in f.readlines():
    i = i.strip()
    out.write(RLPDecode(i))
    out.write('\n')
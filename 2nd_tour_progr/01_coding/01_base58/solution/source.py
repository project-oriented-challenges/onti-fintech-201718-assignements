import bitcoin
n = 10
dict = {}
for i in range(n):
    f = input()
    f = hex(int(f, 2))[2:]
    addition = 40 - len(f)
    f = '0'*addition + f
    f = bitcoin.hex_to_b58check(f)
    for j in f:
        if j in dict:
            dict[j] += 1
        else:
            dict[j] = 1

a = sorted(list(dict.items()), key = lambda x: x[1], reverse=True)
if a[0][0] != '1':
    print(a[0][0])
else:
    print(a[1][0])
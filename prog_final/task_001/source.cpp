#include <bits/stdc++.h>
using namespace std;
typedef pair<int,int> pii;
#define pb push_back
#define sz(x) ((int)x.size())
#define fi first
#define se second


const int MAXN = 100000+10;


vector<int> g[MAXN];
char used[MAXN];
void dfs(int v){
    used[v]= 1;
    for(int to:g[v]){
        if(!used[to]){
            dfs(to);
        }
    }
}

void solve(){
    int n,m;
    cin >> n >> m;
    for(int i=1;i<=m;++i){
        int a,b;
        cin >> a >> b;
        g[a].pb(b);
    }
    int s,t;
    cin >> s >> t;
    dfs(s);
    if(used[t]){
        cout <<"Yes"<<endl;
    } else{
        cout <<"No"<<endl;
    }

}


int main(){
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    solve();

}
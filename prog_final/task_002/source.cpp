#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<int,int> pii;
typedef pair<ll,ll> pll;
#define pb push_back
#define sz(x) ((int)x.size())
#define fi first
#define se second
const ll inf_ll = 2e18;

const int MAXN = 100000+10;


vector<pii> g[MAXN];
ll dis[MAXN];
void dey(int st){
    fill(dis,dis+MAXN,inf_ll);
    dis[st] = 0;
    set<pll> s;

    s.insert({0,st});
    while(!s.empty()){
        int v = s.begin()->se;
        s.erase(s.begin());
        for(pii x:g[v]){
            int to = x.fi;
            int len = x.se;
            if(dis[to]>dis[v]+len){
                s.erase({dis[to],to});
                dis[to]= dis[v]+len;
                s.insert({dis[to],to});
            }
        }
    }
}


void solve(){
int n,m;
    cin >> n >> m;
    for(int i=1;i<=m;++i){
        int a,b,c;
        cin >> a >> b >> c;
        g[a].pb({b,c});
    }
    int s,t;
    cin >> s >> t;
    int q,q0;
    cin >> q >> q0;
    for(int i=1;i<=q;++i){
        int a,b,c;
        cin >> a >> b >> c;
        g[a].pb({b,c+q0});
    }
    dey(s);
    if(dis[t]==inf_ll){
        cout << -1<<endl;
    } else{
        cout <<dis[t]<<endl;
    }

}

int main(){
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    solve();

}
#include <bits/stdc++.h>
using namespace std;
typedef long long ll;
typedef pair<int,int> pii;
typedef pair<ll,ll> pll;
#define pb push_back
#define sz(x) ((int)x.size())
#define fi first
#define se second
const ll inf_ll = 2e18;
const int MAXN = 1e5+1;


vector<pii> g[MAXN];
int val[MAXN];

void dfs(int v,int p){
    for(pii x:g[v]){
        int to = x.fi;
        if(to==p)
            continue;
        int len = x.se;
        val[to] = val[v]^len;
        dfs(to,v);
    }
}

void solve(){
    int n,q;
    cin >> n >> q;
    for(int i=1;i<=n-1;++i){
        int a,b, c;
        cin >> a >> b >> c;
        g[a].pb({b,c});
        g[b].pb({a,c});
    }
    map<int,int> mp;
    dfs(1,-1);

    for(int i=1;i<=n;++i){
        mp[val[i]]++;
    }

    for(int i=1;i<=q;++i){
        int v1,q1;
        cin >> v1>> q1;
        int res = val[v1]^q1;

        cout << mp[res]<<"\n";
    }

}

int main(){
    ios_base::sync_with_stdio(0);
    cin.tie(0);
    cout.tie(0);
    solve();

}
#!/bin/bash

MAINTEXFILE='onti-fintech-201718-assignements.tex'

BUILDDIRNAME='build'

BUILDSCRIPTPATH_IN_DOCKER='./build_in_docker.sh'
MOUNTPOINT_IN_DOCKER='/mnt'
WORKDIR_IN_DOCKER=${MOUNTPOINT_IN_DOCKER}

if [ -f /.dockerenv ]; then
    echo '---=== CONTAINERED ===---'
    ./prereq-actions.sh

    echo "Run PDF compilation"
    latexmk -shell-escape -pdf ${MAINTEXFILE}
    exit 0
fi

echo '---=== STILL ===---'

CURDIR=`pwd`
BUILDDIRPATH=${CURDIR}/${BUILDDIRNAME}

if [ "${1}" == "" ]; then
    if [ ! -d ${BUILDDIRPATH} ]; then
        echo "Create build directory"
        mkdir ${BUILDDIRPATH}
    fi
    echo "Update content of build directory"
    rsync --exclude "/${BUILDDIRNAME}" --exclude '/.*' -av ./ ${BUILDDIRNAME}/
    #content=`ls -1 | grep -v build`
    #for s in ${content}; do
    #    cp -ar ${s} ${CURDIR}/build/
    #done

    echo "Run docker container"
    docker run --rm -it -v ${BUILDDIRPATH}/:${MOUNTPOINT_IN_DOCKER} -w ${WORKDIR_IN_DOCKER} akolotov/latex ${BUILDSCRIPTPATH_IN_DOCKER}
elif [ "${1}" == "clean" ]; then
    if [ -d ${BUILDDIRPATH} ]; then
        echo "Remove build directory"
        rm -rf ${BUILDDIRPATH}
    fi
else
    echo "Incorrect usage"
    exit 1
fi

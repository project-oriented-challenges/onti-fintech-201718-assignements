A = input().split();
n = int(A[0]);
k = int(A[1]);

x = input().split();

for i in range(n) :
    x[i] = int(x[i]);

for i in range(1, n) :
    len = int(x[i]) - int(x[0]);
    cnt = 0;
    now = x[0];
    while (now != x[n - 1]) :
        now += len;
        if now not in x:
            cnt = -1;
            break;
        cnt = cnt + 1;
    if cnt != -1 and cnt <= k :
        print(len);
        break;

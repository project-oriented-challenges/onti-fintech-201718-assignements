from json import dump, load
import os

# install solc by `pip install py-solc`
from solc import compile_files


def delHexPrefix(_s: str):
    if _s[:2] == '0x':
        _s = _s[2:]
    return _s


def writeDataBase(_data, _file):
    with open(_file, 'w') as out:
        dump(_data, out)


def createNewAccount(_w3, _password, _file):
    if os.path.exists(_file):
        os.remove(_file)
    acc = _w3.personal.newAccount(_password)
    data = {"account": acc, "password": _password}
    writeDataBase(data, _file)
    return data['account']


def unlockAccount(_w3, _actor, _pwd):
    _w3.personal.unlockAccount(_actor, _pwd, 60)


def openDataBase(_file):
    if os.path.exists(_file):
        with open(_file) as file:
            return load(file)
    else:
        return None


def getAccountFromDB(_file):
    data = openDataBase(_file)
    if data is not None:
        return data["account"]
    else:
        return None


# TODO: ask an oracle for gas price
def getActualGasPrice(_w3):
    return _w3.toWei(1, 'gwei')


def compileContracts(_files):
    t = type(_files)
    if t == str:
        contracts = compile_files([_files])
    if t == list:
        contracts = compile_files(_files)
    return contracts


def initializeContractFactory(_w3, _compiledContracts, _key, _address=None):
    if _address == None:
        contract = _w3.eth.contract(
            abi=_compiledContracts[_key]['abi'],
            bytecode=_compiledContracts[_key]['bin']
        )
    else:
        contract = _w3.eth.contract(
            abi=_compiledContracts[_key]['abi'],
            address=_address
        )
    return contract


def runCmd(_args, _workdir=''):
    executed = False
    out = ""
    ext_args = _args[:]
    # this will initialize python_intrp when runCmd() will be run first time
    if not hasattr(runCmd, "python_intrp"):
        runCmd.python_intrp = getPythonInterpreter()
    ext_args.insert(0, runCmd.python_intrp)

    if len(_workdir) > 0:
        cwd = os.getcwd()
        os.chdir(_workdir)

    try:
        tmpout = check_output(ext_args, stderr=STDOUT, timeout=50)
    except CalledProcessError as ret:
        tmpout = ret.stdout
    except TimeoutExpired:
        tmpout = ''

    if len(_workdir) > 0:
        os.chdir(cwd)
    if len(tmpout) > 0:
        executed = True
        out = tmpout.decode().splitlines()

    return (executed, out)


accountDatabaseName = 'car.json'
mgmtContractDatabaseName = 'database.json'


def ManagementContract(_w3):
    mgmtContractDatabaseName = 'database.json'
    data = openDataBase(mgmtContractDatabaseName)

    contractFileMgmt = 'contracts/ManagementContract.sol'
    mgmtContractName = 'ManagementContract'
    mgmtContractABI = compileContracts(contractFileMgmt)[f'{contractFileMgmt}:{mgmtContractName}']['abi']
    _address = data['mgmtContract']
    return _w3.eth.contract(abi=mgmtContractABI, address=_address)


def BatteryContract(_w3, _address):
    contractFileBat = 'contracts/BatteryManagement.sol'
    BatContractName = 'BatteryManagement'
    batteryMgmtABI = compileContracts(contractFileBat)[f'{contractFileBat}:{BatContractName}']['abi']
    return _w3.eth.contract(abi=batteryMgmtABI, address=_address)


def DealContract(_w3, _address):
    contractFileDeal = 'contracts/DealInterface.sol'
    DealContractName = 'DealInterface'
    DealContractABI = compileContracts(contractFileDeal)[f'{contractFileDeal}:{DealContractName}']['abi']
    return _w3.eth.contract(abi=DealContractABI, address=_address)

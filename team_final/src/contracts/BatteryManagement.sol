pragma solidity ^0.4.19;

import "./NFT/BasicNFToken.sol";
import "./ManagementContractInterface.sol";
import "./ERC20TokenInterface.sol";
import "./lib/Ownable.sol";
import "./lib/SafeMath.sol";
import "./Fee.sol";
import "./Deal.sol";

contract BatteryManagement is BasicNFToken, Ownable {
    // Для проверки на повторное использование транзакции
    mapping (bytes32 => bool) reused;

    // Для проверки на передачу прав на какую-либо батарею
    // для данного пользователя
    mapping (address => bool) hasBattery;

    // Для проверки на участие батареи в другом контракте
    mapping (bytes20 => bool) isUsed;


    // Временной промежуток запрета разблокирования токенов
    uint256 timeout = 3600;

    // Контракт для хранения амортизационной комиссии
    Fee fees;

    // Для оповещения о передаче прав владения батареей новому владельцу
    // - адрес предыдущего владельца
    // - адрес нового владельца
    // - индентификатор батареи
    event Transfer(address indexed, address indexed, bytes20);


    // Для оповещения после создания контракта на замену батарей
    // - адрес контракта
    event NewDeal(address newDeal);

    ManagementContractInterface public managementContract;
    ERC20TokenInterface public erc20;
    Deal deal;

    // Конструктор контракта
    // - адрес контракта, управляющего списком вендоров.
    // - адрес контракта, управляющего токенами, в которых будет
    //   происходить расчет за замену батарей.
    function BatteryManagement(address _mgmt, address _erc20) public {
        managementContract = ManagementContractInterface(_mgmt);
        erc20 = ERC20TokenInterface(_erc20);
        fees = new Fee();
    }

    // Создает новую батарею
    // Владельцем батареи назначается его текущий создатель.
    // Создание нового батареи может быть доступно только
    // management контракту
    // - адрес производителя батареи
    // - идентификатор батареи
    function createBattery(address _vendor, bytes20 _tokenId) public {
        require(msg.sender == address(managementContract));
        require(!batteryExists(_tokenId));
        _setTokenWithID(_tokenId, _vendor);
        _transfer(0, _vendor, _tokenId);
    }

    // Меняет владельца токена. Может быть вызвана только
    // текущим владельцем
    // - адрес нового владельца
    // - идентификатор батареи
    function transfer(address _to, bytes20 _tokenId) public{
        require(msg.sender == ownerOf[_tokenId]);
        _transfer(msg.sender,_to,_tokenId);
        hasBattery[_to] = true;
        emit Transfer(msg.sender,_to,_tokenId);
    }

    // Меняет владельца для всех токенов перечисленных в списке. Может быть вызвана
    // только текущим владельцем
    // - адрес нового владельца
    // - список идентификаторов батарей
    function transfer(address _to, bytes20[] _tokenIds) public{
        for (uint i = 0 ; i < _tokenIds.length; i++){
            transfer(_to,_tokenIds[i]);
        }
    }

    // Определяет по статусу батареи ее идентификатор, затем текущего владельца.
    // И меняет владельца токена, отвечающего за батаерею. Может быть вызвана
    // только текущим владельцем
    // - запакованные данные, хранящие статус батареи и нового получателя. Упаковка
    //   данных используется для уменьшения используемого газа за данные,
    //   передаваемые в транзакции в метод контракта. Метод упаковки:
    //   p = n*(2**124) + t*(2**192) + v*(2**160) + address
    // - r компонента подписи для данных батареи
    // - s компонента подписи для данных батареи
    //function transfer(bytes32, bytes32, bytes32) public;

    // Делегирует право аккаунту изменять владельца для токена, которым на текущий
    // момент владеет аккаунт-отправитель транзакции. Может выполняться, если
    // аккаунт-отправитель тразанкции действительно владеет данным токенов. Для
    // одного токена может быть только один делегат. Для отзыва права необходимо
    // послать 0 в качестве адреса.
    // Генерирует событие Approval.
    // - адрес делегата
    // - идентификатор токена
    function approve(address _to, bytes20 _tokenId) public{
        require(ownerOf[_tokenId] == msg.sender);
        _approve(_to,_tokenId);
    }

    // Делегирует право аккаунту изменять владельца для токена, которым на текущий
    // момент владеет аккаунт, адрес которого определяется в ходе проверки цифровой
    // подписи. Может выполняться, если аккаунт, восстанавливаемый из цифровой
    // подписи, действительно владеет данным токеном. Для
    // одного токена может быть только один делегат. Для отзыва права необходимо
    // послать 0 в качестве адреса.
    // Цифровая подпись проверяется относительно адреса отправителя транзакции
    // (msg.sender), поскольку подразумевается, что тем самым владелец токена
    // подтверждает, что делегирование права может быть выполнено с данного адреса.
    // Генерирует событие Approval.
    // - адрес делегата
    // - идентификатор токена
    // - v, r, s компоненты цифровой подписи
    function delegatedApprove(address _to, bytes20 _tokenId, uint8 v, bytes32 r, bytes32 s) public{
        address _owner = ecrecover(keccak256(msg.sender),v,r,s);
        require(ownerOf[_tokenId] == _owner);
        _approve(_to,_tokenId);
    }

    // Меняет владельца токена. Может выполняться, только если отправителю
    // транзакций делегировано право изменения владельца для данного токена.
    // Генерирует событие Transfer.
    // - адрес владельца токена
    // - адрес получателя
    // - идентификатор токена
    //function transferFrom(address, address, bytes20) public;

    // Возвращает адрес текущего зарегистрированного владельца батареи
    // - индентификатор батареи
    //function ownerOf(bytes20) view public returns(address);

    // Возвращает адрес производителя батареи
    // - идентификатор батареи
    function vendorOf(bytes20 _batteryId) view public returns(address) {
        return tokenID[_batteryId];
    }

    // Проверяет цифровую подпись для данных и возвращает результат
    // проверки и адрес вендора.
    // Результат проверки: 0 - результат проверки цифровой подписи, показывает
    // что она сделана батареей, для которой существует токен; 1 - транзакция
    // с таким статусом уже отправлялась в блокчейн, что указывает на возможную
    // прослушку траффика; 2 - для  батареи нет соответствующего токена; 999 -
    // другая ошибка.
    // - число зарядов
    // - временная метка
    // - v, r, s компоненты цифровой подписи
    function verifyBattery(uint256 n, uint256 t, uint8 v, bytes32 r, bytes32 s) public view returns(uint256, address) {
        address batteryId = getBatteryIdFromSignature(n, t, v, r, s);
        bytes32 ch = generateStatusUsageHash(batteryId, n, t);
        uint256 retval = 999;
        if (batteryExists(bytes20(batteryId))) {
            if (reused[ch]) {
                retval = 1;
            }
            else {
                retval = 0;
            }
        } else {
            retval = 2;
        }
        address vendorId = vendorOf(bytes20(batteryId));
        return (retval, vendorId);
    }

    // Формирует новый контракт на выполнение операций по замене батарей.
    // В контракт передаются идентификаторы батарей, участвующие в сделке,
    // адрес аккаунта автомобиля, сумма на компенсацию амортизации батарей,
    // сумма за выполенние работ по замене.
    // Идентификаторы батарей становятся известны в ходе проверки цифровых
    // подписей для статусов батарей.
    // Сумма компенсации исчисляется в зависимости от количества заряда,
    // переданного в статусе батарей.
    // Контракт не должен создаваться, если для батарей с такими
    // идентфикаторами нет соответствующего токена, если одна из батарей
    // уже участвует в каком-то другом контракте замены или участники контракта
    // не являются владельцами батарей.
    // - запакованные данные, хранящие статус сразу двух батарей. Упаковка
    //   данных используется для уменьшения используемого газа за данные,
    //   передаваемые в транзакции в метод контракта. Метод упаковки:
    //   p = n1*(2**160) + t1*(2**128) + v1*(2**96) + n2*(2**64) + t2*(2**32) + v2
    // - r компонента подписи для данных старой батареи
    // - s компонента подписи для данных старой батареи
    // - r компонента подписи для данных новой батареи
    // - s компонента подписи для данных новой батареи
    // - адрес аккаунта автомобиля
    // - количество расчетных токенов, определяющих выполнение работ по замене
    function initiateDeal(
        uint256 _p,
        bytes32 _r1,
        bytes32 _s1,
        bytes32 _r2,
        bytes32 _s2,
        address _car,
        uint256 _scenterFee
    )
    public
    {
        uint256 p = _p;
        bytes20 oldBatteryId = verifyOldBattery(_p, _r1, _s1, _car);
        bytes20 newBatteryId = verifyNewBattery(_p%2**96, _r2, _s2);
        uint256 amortizedFee = getAmortizedFee(getValueFromPack(p, 2**160), getValueFromPack(p%2**96, 2**64));
        deal = new Deal(oldBatteryId, newBatteryId, erc20, amortizedFee, _scenterFee, timeout);
        isUsed[oldBatteryId] = true;
        isUsed[newBatteryId] = true;
        reused[generateStatusUsageHash(address(oldBatteryId), getValueFromPack(p, 2**160), getValueFromPack(p%2**160, 2**128))] = true;
        reused[generateStatusUsageHash(address(newBatteryId), getValueFromPack(p%2**96, 2**64), getValueFromPack(p%2**64, 2**32))] = true;
        approve(address(deal), newBatteryId);
        emit NewDeal(address(deal));
    }

    // Возвращает количество зарядов для конкретной батареи, зарегистрированных
    // в данный момент
    // - идентификатор батареи
    //function chargesNumber(bytes20) view public returns(uint256);

    // Извлекает из переданных данных цифровую подпись текущего владельца батареи,
    // адрес нового владельца батареи, ее статус. Изменяет владельца, если
    // нет нарущений прав владения для восстановленного из подпсис адреса.
    // - запакованные данные, хранящие статус батареи и нового получателя. Упаковка
    //   данных используется для уменьшения используемого газа за данные,
    //   передаваемые в транзакции в метод контракта. Метод упаковки (от старших
    //   байт к младшим):
    //    0..3   байт - число зарядов
    //    4..7   байт - временная метка
    //    8..75  байт - v, r, s компоненты цифровой подписи статуса батареи
    //   76..95  байт - адрес нового владельца
    //   96..163 байт - v, r, s компоненты цифровой подписи текущего владельца
    //function approvedTransfer(bytes) public;

    // Проверяет переданный адрес, если он является контрактом сделки.
    // Возрващает 0 - контракт сделки существует и аккаунт, с которого
    // осуществляется запрос, является участником сделки; 1 - контракт сделки
    // существует, но аккаунт не является участников сделки; 2 - контракт
    // сделки не существует.
    //function checkDealContract(address) view public returns(uint256);

    // Устанавливает верменной промежуток, после которого разрешено
    // разблокирование расчетных токенов контрактом сделки. Данная операция
    // разрешена только аккаунту производителя ПО.
    // - размер временного промежутка в секундах.
    function setTimeoutThreshold(uint256 _timeout) public onlyOwner {
        timeout = _timeout;
    }

    // Возвращает временной промежуток, запрета разблокирования расчетных
    // токенов, установленный на текущий момент.
    //function timeoutThreshold() view public returns(uint256);

    // Устанавливает значения минимальной комиссии
    // за амортизационную стоимость батарей
    // - значения в зависимости от разности батарей
    // - отступ, с которого необходимо устанавливать комиссию
    function setMinimalFees(uint256[] _fees, uint counter) public {
        fees.setMinimalFees(_fees, counter);
    }

    // Устанавливает значения максимальной комиссии
    // за амортизационную стоимость батарей
    // - значения в зависимости от разности батарей
    // - отступ, с которого необходимо устанавливать комиссию
    function setMaximalFees(uint256[] _fees, uint counter) public {
        fees.setMaximalFees(_fees, counter);
    }

    // TODO: only for testing. Delete before release
    function getMaximalFees(uint256 n) public returns(uint256) {
        return fees.maximalFees(n);
    }

    // Высчитывает хэш, используемый для проверки на повторное
    // использование транзакции
    // - адрес, полученный из подписи и хэша транзакции
    // - количество заряда батареи
    // - время получения информации о батарее
    function generateStatusUsageHash(address _addr, uint256 _n, uint256 _t) internal pure returns (bytes32) {
        return keccak256(_addr, _n, _t);
    }

    // Вычисляет Id батареи по информации из её прошивки
    // - количество зарядок
    // - время получения информации
    // - v компонента подписи для батареи
    // - r компонента подписи для батареи
    // - s компонента подписи для батареи
    function getBatteryIdFromSignature(uint256 _n, uint256 _t, uint8 _v, bytes32 _r, bytes32 _s)
        public
        pure
        returns (address batteryId)
    {
        uint256 message = 2**32 * _n + _t;
        bytes32 h = keccak256(message);
        batteryId = ecrecover(h, _v, _r, _s);
    }

    // Проверяет зарегистрирован ли токен с таким идентификатором любым из производителей.
    function batteryExists(bytes20 _batteryId) internal view returns (bool) {
        return tokenID[_batteryId] != address(0);
    }

    // Расчитывает амортизационную стоимость при замене батареи
    function getAmortizedFee(uint256 _oldBattery, uint256 _newBattery) internal view returns(uint256) {
        if (_newBattery >= 300) {
            return 0;
        } else if (_oldBattery >= 300) {
            return 1000;
        } else if (_newBattery >= 150) {
            return _oldBattery - _newBattery;
        } else if (_newBattery >= 50) {
            if (_oldBattery >= 150) {
                return fees.minimalFees(_oldBattery - _newBattery);
            }
            else {
                return _oldBattery - _newBattery;
            }
        } else if (_oldBattery >= 150) {
            return fees.maximalFees(_oldBattery - _newBattery);
        } else if (_oldBattery >= 50) {
            return fees.minimalFees(_oldBattery - _newBattery);
        } else {
            return _oldBattery - _newBattery;
        }
    }

    // Проверяет батарею предоставленную машиной
    function verifyOldBattery(
        uint256 _p,
        bytes32 _r1,
        bytes32 _s1,
        address _car
    )
    internal view returns(bytes20) {
        uint256 n = SafeMath.div(_p, 2**160);
        _p %= 2**160;
        uint256 t = SafeMath.div(_p, 2**128);
        _p %= 2**128;
        uint256 v = SafeMath.div(_p, 2**96);
        _p %= 2**96;

        require(managementContract.cars(_car));
        uint256 oldBatteryValidity;
        address oldBatteryVendorAddress;
        (oldBatteryValidity, oldBatteryVendorAddress) = verifyBattery(n, t, uint8(v), _r1, _s1);
        require(oldBatteryValidity == 0);
        address oldBatteryId = getBatteryIdFromSignature(n, t, uint8(v), _r1, _s1);
        require(ownerOf[bytes20(oldBatteryId)] == _car);
        require(hasBattery[_car]);
        require(!isUsed[bytes20(oldBatteryId)]);

        return(bytes20(oldBatteryId));
    }

    // Проверяет батарею предоставленную сервисным центром
    function verifyNewBattery(
        uint256 _p,
        bytes32 _r2,
        bytes32 _s2
    )
    internal view returns(bytes20) {
        uint256 n = SafeMath.div(_p, 2**64);
        _p %= 2**64;
        uint256 t = SafeMath.div(_p, 2**32);
        _p %= 2**32;
        uint256 v = _p;

        require(managementContract.serviceCenters(msg.sender));
        uint256 newBatteryValidity;
        address newBatteryVendorAddress;
        (newBatteryValidity, newBatteryVendorAddress) = verifyBattery(n, t, uint8(v), _r2, _s2);
        require(newBatteryValidity == 0);
        address newBatteryId = getBatteryIdFromSignature(n, t, uint8(v), _r2, _s2);
        require(ownerOf[bytes20(newBatteryId)] == msg.sender);
        require(hasBattery[msg.sender]);
        require(!isUsed[bytes20(newBatteryId)]);

        return bytes20(newBatteryId);
    }

    // Получает значение из упакованных данных с необходимым сдвигом
    function getValueFromPack(uint256 _p, uint256 _power) internal pure returns(uint256) {
        return SafeMath.div(_p, _power);
    }

}

pragma solidity ^0.4.19;

import "./ServiceProviderWallet.sol";
import "./BatteryManagementInterface.sol";
import "./lib/Ownable.sol";

contract ManagementContract is Ownable {

    //Депозит для каждого вендора
    mapping (address => uint256) public vendorDeposit;

    //По адресу отправителя определяет имя производителя которое ему принадлежит
    mapping (address => bytes4) public vendorId;

    //По индефикатору возвращает имя производителя
    mapping (bytes4 => bytes) public vendorNames;

    //Проверка уже зарегестрированного имени
    mapping (bytes => bool) registeredVendor;

    //Проверка уже зарегестрированного адреса производителя
    mapping (address => bool) public registeredVendorAddress;

    //По адресу возвращает batteryFee для данного производителя
    mapping (address => uint256) vendorBatteryFee;

    // Возвращает истину или ложь в зависимости от того, зарегистрирован
    // электромобиль с указанным адресом в системе или нет.
    mapping (address => bool) public serviceCenters;

    // Возвращает истину или ложь в зависимости от того, зарегистрирован
    // электромобиль с указанным адресом в системе или нет.
    mapping (address => bool) public cars;

    // Контракт управляющий информацией о батареях
    BatteryManagementInterface public batteryManagement;
    // Контракт кошелька
    ServiceProviderWallet public walletContract;

    //Цена за создание одной батареи
    uint256 BatFee;
    // Для оповещения регистрации нового производителя
    // - адрес аккаунта из-под которого проходила регистрация
    // - идентификатор производителя
    event Vendor(address owner, bytes4 tokenId);

    // Для оповещения о создании новой батареи
    // - идентификатор производителя
    // - идентификатор батареи
    event NewBattery(bytes4 vendorId, bytes20 tokenId);

    // Конструктор контракта
    // - адрес контракта, ответственного за накопление криптовалюты,
    //   перечисляемой в качестве депозита за использование сервиса.
    // - сумму сбора за выпуск одной батареи
    function ManagementContract(address _wallet, uint256 _batfee) public{
        BatFee = _batfee;
        walletContract = ServiceProviderWallet(_wallet);
    }

    // Устанавливает адрес для контракта, ответственного за
    // управление информацией о батареях.
    // Доступен только создателю management контракта
    // - адрес контракта, управляющего инфорацией о батареях
    function setBatteryManagementContract(address _batmgmt) onlyOwner public {
        batteryManagement = BatteryManagementInterface(_batmgmt);
    }

    // Регистрирует вендора, если при вызове метода перечисляется
    // достаточно средств.
    // - наименование производителя
    function registerVendor(bytes _name) public payable{
        require(msg.value >= BatFee*1000);
        require(!registeredVendor[_name]);
        registeredVendor[_name] = true;
        require(!registeredVendorAddress[msg.sender]);
        registeredVendorAddress[msg.sender] = true;
        vendorBatteryFee[msg.sender] = BatFee;

        bytes4 _nameSym = bytes4(keccak256(msg.sender,_name,block.number));
        vendorDeposit[msg.sender] += msg.value;
        address(walletContract).transfer(msg.value);

        vendorId[msg.sender] = _nameSym;
        vendorNames[_nameSym] = _name;

        Vendor(msg.sender,_nameSym);
    }

    // Регистрирует новые батареи, если при вызове метода на балансе
    // данного производителя достаточно средств. Во время регистрации
    // батарей баланс уменьшается соответственно количеству батареи и
    // цене, установленной для данного производителя на текущий момент.
    // - идентификаторы батарей
    function registerBatteries(bytes20[] _ids) public payable{
        uint _n = _ids.length;
        require(msg.value + vendorDeposit[msg.sender] >= _n * vendorBatteryFee[msg.sender]);
        bytes4 _tokenId = vendorId[msg.sender];
        require(_tokenId != "");
        vendorDeposit[msg.sender] += msg.value - (_n * vendorBatteryFee[msg.sender]);
        if (msg.value > 0){
            address(walletContract).transfer(msg.value);
        }
        for (uint i=0; i < _n; i++){
            batteryManagement.createBattery(msg.sender, _ids[i]);
            NewBattery(_tokenId, _ids[i]);
        }

    }

    // Устанавливает сумму сбора за выпуск одной батареи
    // - сумма сбора в wei
    function setFee(uint256 pr) public onlyOwner {
        BatFee = pr;
    }

    // Возвращает сумму сбора в wei, который будет списываться с депозита
    // за каждую выпущенную батарею. Если запрос идет от зарегистрированного
    // производителя батарей, то выдается та сумма сбора, которая была на момент
    // его регистрации.
    function batteryFee() public view returns(uint256){
        if (vendorBatteryFee[msg.sender] != 0) {
            return vendorBatteryFee[msg.sender];
        }
        return BatFee;
    }

    // Возвращает сумму сбора в wei, которую необходимо перечислить в качестве
    // депозита при регистрации производителя батарей. Сумма депозита
    // равна сумме за регистрацию 1000 батарей, что позволит защититься от
    // мошенников, поскольку требует серьезных вложений.
    function registrationDeposit() public view returns(uint256){
        return 1000 * BatFee;
    }

    // Регистрирует в системе адрес отправителя транзакции, как сервис центр.
    // Регистрация просиходит только если данный адрес уже не был зарегистрирован
    // в качестве сервис центра или электромобиля.
    function registerServiceCenter() public{
        require(!cars[msg.sender]);
        require(!serviceCenters[msg.sender]);
        serviceCenters[msg.sender] = true;
    }

    // Регистрирует в системе адрес отправителя транзакции, электромобиль.
    // Регистрация просиходит только если данный адрес уже не был зарегистрирован
    // в качестве сервис центра или электромобиля.
    function registerCar() public{
        require(!cars[msg.sender]);
        require(!serviceCenters[msg.sender]);
        cars[msg.sender] = true;
    }

    function vendorNamesDataLen(bytes4 _id) public view returns(uint256) {
        return(vendorNames[_id].length);
    }

    function vendorNamesDataChunk(bytes4 _id, uint256 n) public view returns(bytes32){
        uint256 res = 0;
        uint256 i = 0;
        while((i<32) && (i<(vendorNamesDataLen(_id)-n*32))) {
            res = res + uint256(vendorNames[_id][i+n*32]) * 2 ** ((31-i)*8);
            i++;
        }
        return(bytes32(res));
    }

}

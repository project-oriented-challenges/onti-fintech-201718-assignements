pragma solidity ^0.4.21;

import "./erc20/MintableToken.sol";

contract ERC20Token is MintableToken {

    function ERC20Token() public {
        mint(owner, 100 ether);
    }
}

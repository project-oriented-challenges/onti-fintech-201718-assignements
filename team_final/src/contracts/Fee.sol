pragma solidity ^0.4.19;

contract Fee {
    // Для хранения минимальных компенсаций в зависимости
    // от разницы количества зарядок батарей
    mapping (uint256 => uint256) public minimalFees;

    // Для хранения минимальных компенсаций в зависимости
    // от разницы количества зарядок батарей
    mapping (uint256 => uint256) public maximalFees;

    function setMinimalFees(uint256[] _fees, uint counter) public {
        for (uint i = counter; i<_fees.length+counter; i++) {
            minimalFees[i] = _fees[i-counter];
        }
    }

    function setMaximalFees(uint256[] _fees, uint counter) public {
        for (uint i = counter; i<_fees.length+counter; i++) {
            maximalFees[i] = _fees[i-counter];
        }
    }
}
pragma solidity ^0.4.19;

import "./BatteryManagementInterface.sol";
import "./ManagementContract.sol";

contract Deal {

    BatteryManagementInterface batteryContract;
    ManagementContract managementContract;

    bytes20 oldBat;
    bytes20 newBat;
    address erc20;
    uint256 ammortizedFee;
    uint256 serviceFeeValue;
    uint256 timeout;
    uint256 deal_state;

    // Конструктор контракта
    // - идентификатор старой батареи
    // - идентификатор новой батареи
    // - адрес контракта расчетных токенов
    // - количество токенов, определяющих компенсацию амортизации батарей
    // - количество токенов, определяющих выполнение работ по замене
    // - временной промежуток, в ходе которого запрещено разблокирование
    //   расчетных токенов.
    function Deal(bytes20 _oldBat, bytes20 _newBat, address _erc20, uint256 _ammorFee, uint256 _serviceFee, uint256 _timeout) public {
        oldBat = _oldBat;
        newBat = _newBat;
        erc20 = _erc20;
        ammortizedFee = _ammorFee;
        serviceFeeValue = _serviceFee;
        timeout = _timeout;
        deal_state = 1;
        batteryContract = BatteryManagementInterface(msg.sender);
        managementContract = ManagementContract(batteryContract.managementContract());
    }

    // Возвращают идентификаторы батарей, вовлеченных в контракт.
    function oldBattery() view public returns(bytes20) {
        return oldBat;
    }
    function newBattery() view public returns(bytes20) {
        return newBat;
    }

    // Возвращают информацию о батареях, вовлеченных в контракт.
    // Возвращаемые значения:
    // - количество зарядов
    // - идентификатор производителя
    // - наименование производителя
    function oldBatteryInfo() view public returns(uint256, bytes4, bytes)
    {
        uint256 n = batteryContract.chargesNumber(oldBat);
        address vendorAddress = batteryContract.vendorOf(oldBat);
        bytes4 vendorId = managementContract.vendorId(vendorAddress);
        //bytes vendorName = ;

        return (n, vendorId, getVendorNamesByChunks(vendorId));
    }

    function newBatteryInfo() view public returns(uint256, bytes4, bytes) {
        uint256 n = batteryContract.chargesNumber(newBat);
        address vendorAddress = batteryContract.vendorOf(newBat);
        bytes4 vendorId = managementContract.vendorId(vendorAddress);
        //bytes vendorName = getVendorNamesByChunks(vendorId);

        return (n, vendorId, getVendorNamesByChunks(vendorId));
    }

    // Возвращает количество токенов, определяющих компенсацию амортизации
    // батарей
    function deprecationValue() view public returns(uint256) {
        return ammortizedFee;
    }

    // Возвращает количество токенов, определяющих выполнение работ по
    // замене
    function serviceFee() view public returns(uint256) {
        return serviceFeeValue;
    }

    // Возвращает текущий статус контракта
    // 1 - Ожидание согласия со сделкой от автомобиля
    // 2 - Согласие со сделкой получено
    // 3 - Оплата работ выполнена
    function state() view public returns(uint256) {
        return deal_state;
    }

    // Проверяет, что переданный статус батареи получен от батареи, которая
    // зарегистрирована к замене (старая), что данный запрос идет от текущего
    // владельца батареи, который участвует в контракте в роли получателя новой
    // батареи. Если все условия выполняются, то токены идущие в оплату
    // контракта переводятся на счет контракта.
    // - запакованные данные, хранящие статус батареи. Упаковка
    //   данных используется для уменьшения используемого газа за данные,
    //   передаваемые в транзакции в метод контракта. Метод упаковки:
    //   p = n*(2**64) + t*(2**32) + v
    // - r компонента подписи для данных батареи
    // - s компонента подписи для данных батареи
    // function agreeToDeal(uint256, bytes32, bytes32) public;

    // Проверяет, что переданный статус батареи получен от батареи, на которую
    // должна происходить замена. При этом:
    // 1) либо данный запрос идет от аккаунта, который участвует в контракте в роли
    //    получателя новой батареи
    // 2) либо запрос идет от аккаунта, который участвует в контракте в роли того,
    //    кто предоставляет сервис по замене и с момента запроса согласия с
    //    контрактом  прошло 24 часа
    // Если все условия выполняются, то токены идущие в оплату контракта
    // переводятся на счет сервисного центра, владельцы батарей изменяются.
    // - запакованные данные, хранящие статус батареи. Упаковка
    //   данных используется для уменьшения используемого газа за данные,
    //   передаваемые в транзакции в метод контракта. Метод упаковки:
    //   p = n*(2**64) + t*(2**32) + v
    // - r компонента подписи для данных батареи
    // - s компонента подписи для данных батареи
    // function confirmDeal(uint256, bytes32, bytes32) public;

    // Перепосылает аргументы в контракт BatteryManagement для проверки цифровой
    // подписи. Возвращаемый результат проверки такой же, как и в оригинальной
    // функции: 0 - результат проверки цифровой подписи, показывает
    // что она сделана батареей, для которой существует токен; 1 - транзакция
    // с таким статусом уже отправлялась в блокчейн, что указывает на возможную
    // прослушку траффика; 2 - для  батареи нет соответствующего токена; 999 -
    // другая ошибка.
    // - число зарядов
    // - временная метка
    // - v, r, s компоненты цифровой подписи
    function verifyBattery(uint256 n, uint256 t, uint8 v, bytes32 r, bytes32 s) public view returns(uint256, address) {
        return batteryContract.verifyBattery(n, t, v, r, s);
    }

    // Обрабатывает запрос на разблокирование расчетных токенов и возвращает
    // токены на счет электромобиля, владельцы батарей, участвующих в сделке,
    // не изменяются.
    // Выполняется только если отправитель запроса, зарегистрирован в качестве
    // получателя новой батареи, если проверка подписи из статуса батареи указывает
    // на то, что батарея не была заменена, и если время из статуса больше,
    // чем разрешенное время разблокировки токенов.
    // - запакованные данные, хранящие статус батареи. Упаковка
    //   данных используется для уменьшения используемого газа за данные,
    //   передаваемые в транзакции в метод контракта. Метод упаковки:
    //   p = n*(2**64) + t*(2**32) + v
    // - r компонента подписи для данных батареи
    // - s компонента подписи для данных батареи
    // function releaseTokensByCar(uint256, bytes32, bytes32) public;

    // Обрабатывает запрос на разблокирование расчетных токенов и перечисляет
    // токены на счет сервисного центра, происходит смена владельцев батарей,
    // участвующих в сделке.
    // Выполняется только если отправитель запроса, зарегистрирован в качестве
    // изначального владельца новой батареи, если проверка подписи из статуса
    // батареи указывает на то, что батарея действительно была заменена, и
    // если время из статуса больше, чем разрешенное время разблокировки токенов.
    // - запакованные данные, хранящие статус батареи. Упаковка
    //   данных используется для уменьшения используемого газа за данные,
    //   передаваемые в транзакции в метод контракта. Метод упаковки:
    //   p = n*(2**64) + t*(2**32) + v
    // - r компонента подписи для данных батареи
    // - s компонента подписи для данных батареи
    // function releaseTokensByServiceCenter(uint256, bytes32, bytes32) public;

    // Отменяет сделку, если она получатель новой батареи, не согласился с
    // контрактом. Выполняется только если отправитель запроса - сервисный
    // центр.
    // function cancelDeal() public;

    function getVendorNamesByChunks(bytes4 _id) public view returns(bytes){
        uint256 l = managementContract.vendorNamesDataLen(_id);
        bytes memory data = new bytes(l);

        uint256 pos = 0;
        for(uint256 i=0; i<(l/32)+1; i++) {
            bytes32 d = managementContract.vendorNamesDataChunk(_id, i);
            uint256 j=0;
            while(j<32 && j+pos<l) {
                data[pos+j] = d[j];
                j++;
            }
            pos = pos + j;
        }

        return(data);
    }
}

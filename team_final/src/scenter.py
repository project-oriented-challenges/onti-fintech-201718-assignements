import warnings

warnings.simplefilter("ignore", category=DeprecationWarning)

# install web3 >=4.0.0 by `pip install --upgrade 'web3==4.0.0b11'`
from web3 import Web3
from web3.middleware import geth_poa_middleware
from web3.utils.transactions import wait_for_transaction_receipt

from json import loads

import argparse, subprocess
import sys, os

from utils import createNewAccount, openDataBase, unlockAccount, getActualGasPrice, \
    compileContracts, initializeContractFactory, ManagementContract, BatteryContract, DealContract

from verify_battery import verify, return_ntvrs_from, batteryExists

web3 = Web3()

# configure provider to work with PoA chains
web3.middleware_stack.inject(geth_poa_middleware, layer=0)

accountDatabaseName = 'scenter.json'
mgmtContractDatabaseName = 'database.json'

gasPrice = getActualGasPrice(web3)

registrationRequiredGas = 50000


def _getBatteryStatus(_path):
    if not os.path.exists(_path):
        return

    args = ['python', _path, '--getJson']

    batteryOut = subprocess.Popen(args, stdout=subprocess.PIPE)
    batteryStatus = loads(batteryOut.communicate()[0].decode())

    return batteryStatus


def reg():
    data = openDataBase(mgmtContractDatabaseName)
    if data is None:
        print("Cannot access management contract database")
        return


    data = openDataBase(accountDatabaseName)
    if data is None:
        print("Cannot access account database")
        return

    actor = data["account"]

    tx = {'from': actor, 'gasPrice': gasPrice}

    mgmtContract = ManagementContract(web3)

    if registrationRequiredGas * gasPrice > web3.eth.getBalance(actor):
        print("No enough funds to send transaction")
        return

    unlockAccount(web3, actor, data["password"])

    try:
        txHash = mgmtContract.functions.registerServiceCenter().transact(tx)
    except ValueError:
        print("Already registered")
        return

    receipt = wait_for_transaction_receipt(web3, txHash)
    if receipt.status == 1:
        print("Registered successfully")


def createDeal(_bat_new, _bat_old, _car, _service_price):
    data = openDataBase(accountDatabaseName)
    if data is None:
        print("Cannot access account database")
        return

    actor = data["account"]

    tx = {'from': actor, 'gasPrice': gasPrice}

    unlockAccount(web3, actor, data["password"])

    if not batteryExists(_bat_old):
        print('Cannot access battery')
        return
    n1, t1, v1, r1, s1 = return_ntvrs_from(_bat_old)

    if not batteryExists(_bat_new):
        print('Cannot access battery')
        return
    n2, t2, v2, r2, s2 = return_ntvrs_from(_bat_new)

    p = n1 * (2 ** 160) + t1 * (2 ** 128) + v1 * (2 ** 96) + n2 * (2 ** 64) + t2 * (2 ** 32) + v2

    mgmtContract = ManagementContract(web3)
    _bat_contract_address = mgmtContract.functions.batteryManagement().call()
    batContract = BatteryContract(web3, _bat_contract_address)

    try:
        txHash = batContract.functions.initiateDeal(p, web3.toBytes(hexstr=r1), web3.toBytes(hexstr=s1), web3.toBytes(hexstr=r2), web3.toBytes(hexstr=s2), _car, _service_price).transact(tx)
        receipt = wait_for_transaction_receipt(web3, txHash)
        if receipt.status == 0:
            print("Deal was not created.")
            return
    except BaseException as error:
        if error.args[0]['message'] == 'insufficient funds for gas * price + value':
            print('No enough funds to send transaction')
            return
        print("Deal was not created.")
        return

    # Contract.events.<event name>.createFilter(fromBlock=block, [toBlock=block, argument_filters={"arg1": "value"}
    logs = batContract.events.NewDeal().processReceipt(receipt)[::-1]
    for log in logs:
        try:
            if log['args']['newDeal']:
                address_new_deal = log['args']['newDeal']
                print(f"Deal: {address_new_deal}")
                return
        except:
            return


def create_parser():
    parser = argparse.ArgumentParser(
        description='Service provider tool',
        epilog="""
    It is expected that Web3 provider specified by WEB3_PROVIDER_URI
    environment variable. E.g.
    WEB3_PROVIDER_URI=file:///path/to/node/rpc-json/file.ipc
    WEB3_PROVIDER_URI=http://192.168.1.2:8545
    """
    )

    parser.add_argument(
        '--new', type=str, required=False,
        help='Add new service scenter account'
    )

    parser.add_argument(
        '--reg', action='store_true', required=False,
        help='Register service scenter in the chain'
    )

    parser.add_argument(
        '--verify', type=str, required=False,
        help='Verify battery on validity'
    )

    parser.add_argument(
        '--contract', nargs=4, required=False,
        help='Create deal between car and service center'
    )

    return parser


def main():
    parser = create_parser()
    args = parser.parse_args()

    if args.new:
        print(createNewAccount(web3, args.new, accountDatabaseName))
    elif args.reg:
        reg()
    elif args.verify:
        if batteryExists(args.verify):
            result = verify(web3, args.verify)
            if result[0] == 0:
                n = result[1]
                vendorId = result[2]
                vendorName = result[3]
                print("Verified successfully.")
                print('Total charges: {}'.format(n))
                print('Vendor ID: {}'.format(vendorId))
                print('Vendor Name: {}'.format(vendorName))
            elif result[0] == 1:
                print('Battery with the same status already replaced. Probably the battery forged.')
            elif result[0] == 2:
                print('Verifiсation failed. Probably the battery forged.')
            else:
                print('Error. Cannot verify battery.')
        else:
            print('Cannot access battery')

    elif args.contract:
        bat_old = args.contract[0]
        bat_new = args.contract[1]
        car = args.contract[2]
        service_price = int(args.contract[3])
        createDeal(bat_old, bat_new, car, service_price)
    else:
        sys.exit("No parameters provided")


if __name__ == '__main__':
    main()

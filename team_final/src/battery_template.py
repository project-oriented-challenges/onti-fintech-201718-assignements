import sys, os, datetime as dt
from py_ecc.secp256k1 import ecdsa_raw_sign
from json import load, dump, dumps
from sha3 import keccak_256
import argparse

def _getDatabaseName(_args):
    return os.path.splitext(_args)[0] + ".json"

def _writeDataBase(_data, _file):
    with open(_file, 'w') as out:
        dump(_data, out)

def _openDataBase(_file):
    if os.path.exists(_file):
        with open(_file) as file:
            return load(file)
    else:
        return None

def _zpad(_x, _l, _s):
    return _s * (_l - len(_x)) + _x

def _getBatteryStatus(_file):
    file = _getDatabaseName(_file)

    databaseName = _getDatabaseName(file)
    data = _openDataBase(databaseName)
    if data is None:
        n = 0
    else:
        n = data['n']

    t = int(dt.datetime.utcnow().timestamp())
    priv = int(_privateKey, 16).to_bytes(32, 'big')
    m = n * 2 ** 32 + t

    message = m.to_bytes(32, byteorder='big')
    data = keccak_256(message).digest()
    batteryId = ecdsa_raw_sign(data, priv)

    v = batteryId[0]
    r = _zpad(hex(batteryId[1])[2:], 64, '0')
    s = _zpad(hex(batteryId[2])[2:], 64, '0')

    return n, t, v, r, s

def get(_file):
    n, t, v, r, s = _getBatteryStatus(_file)
    print(n, t, v, r, s, sep = '\n')

def charge(_file):
    databaseName = _getDatabaseName(_file)
    data = _openDataBase(databaseName)
    if data is None:
        data = {'n': 0}

    data['n'] = data['n'] + 1

    _writeDataBase(data, databaseName)

def getJson(_file):
    n, t, v, r, s = _getBatteryStatus(_file)
    data = {'n': n, 't': t, 'v': v, 'r': r, 's':s}
    print(dumps(data))

_privateKey = 'cafecafecafecafecafecafecafecafecafecafecafecafecafecafecafecafe'

def create_parser():
    parser = argparse.ArgumentParser(
        description='Service provider tool',
        epilog="""
    It is expected that Web3 provider specified by WEB3_PROVIDER_URI
    environment variable. E.g.
    WEB3_PROVIDER_URI=file:///path/to/node/rpc-json/file.ipc
    WEB3_PROVIDER_URI=http://192.168.1.2:8545
    """
    )

    parser.add_argument(
        '--get', action='store_true', required=False,
        help='Return battery status'
    )

    parser.add_argument(
        '--charge', action='store_true', required=False,
        help='Increase counter of charges'
    )

    parser.add_argument(
        '--getJson', action='store_true', required=False,
        help='Return battery status in JSON format'
    )

    return parser

def main():
    parser = create_parser()
    args = parser.parse_args()

    if args.get:
        get(sys.argv[0])
    elif args.charge:
        charge(sys.argv[0])
    elif args.getJson:
        getJson(sys.argv[0])
    else:
        sys.exit("No parameters provided")

if __name__ == '__main__':
    main()
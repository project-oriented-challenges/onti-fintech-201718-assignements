# install web3 >=4.0.0 by `pip install --upgrade 'web3==4.0.0b11'`
from web3 import Web3, IPCProvider
from web3.middleware import geth_poa_middleware
from web3.utils.transactions import wait_for_transaction_receipt

# install solc by `pip install py-solc`
from solc import compile_files

from json import load, dump, JSONDecodeError
import argparse
import sys, os
from utils import ManagementContract, BatteryContract, delHexPrefix

web3 = Web3()

# configure provider to work with PoA chains
web3.middleware_stack.inject(geth_poa_middleware, layer=0)
try:
    with open('account.json') as file:
        config = load(file)
    actor = web3.toChecksumAddress(config['account'])
    gasPrice = web3.toWei(1, 'gwei')
    txTmpl = {'from': actor, 'gasPrice': gasPrice}
except:
    pass

databaseFile = 'database.json'

try:
    with open(databaseFile) as file:
        database = load(file)
except FileNotFoundError:
    database = {}
except JSONDecodeError:
    database = {}


def unlockAccount(_w3):
    _w3.personal.unlockAccount(actor, config['password'], 300)


def compileContracts(_files):
    t = type(_files)
    if t == str:
        contracts = compile_files([_files])
    if t == list:
        contracts = compile_files(_files)
    return contracts


def initializeContractFactory(_w3, _compiledContracts, _key, _address=None):
    if _address == None:
        contract = _w3.eth.contract(
            abi=_compiledContracts[_key]['abi'],
            bytecode=_compiledContracts[_key]['bin']
        )
    else:
        contract = _w3.eth.contract(
            abi=_compiledContracts[_key]['abi'],
            address=_address
        )
    return contract


def writeDatabase(databaseFile='database.json'):
    with open(databaseFile, 'w') as file:
        dump(database, file)


def safetySendTx(_w3, _tx, _args):
    mgmtContract = ManagementContract(_w3)
    _gasLimit = _w3.eth.getBlock('latest')['gasLimit']
    _batSuccess = []
    # _args is list of batIds
    start, end = 0, len(_args)
    flag = True

    while flag:
        try:
            Gas = mgmtContract.functions.registerBatteries(_args[start:end]).estimateGas(_tx)

            if Gas < int(_gasLimit / 2):
                flag = False
        except:
            pass
        end //= 2
    end = max(end, 1)
    for it in range(0, len(_args), end):
        _to = min(len(_args), it + end)
        txHash = mgmtContract.functions.registerBatteries(_args[it:_to]).transact(_tx)
        _tx['value'] = 0
        receipt = wait_for_transaction_receipt(_w3, txHash)
        if receipt.status == 1:
            _batSuccess += _args[it:_to]
        else:
            break

    # Total
    status = 0 if (len(_batSuccess) == 0) else 1
    status = 2 if (len(_batSuccess) == len(_args)) else status

    return status, _batSuccess


def regfee(_w3):
    mgmtContract = ManagementContract(_w3)
    result = mgmtContract.functions.registrationDeposit().call()
    return _w3.fromWei(result, 'ether')


def batfee(_w3):
    tx = txTmpl
    mgmtContract = ManagementContract(_w3)
    result = mgmtContract.functions.batteryFee().call({'from': tx['from']})
    return _w3.fromWei(result, 'ether')


def deposit(_w3):
    tx = txTmpl
    mgmtContract = ManagementContract(_w3)
    if mgmtContract.functions.registeredVendorAddress(tx['from']).call():
        result = mgmtContract.functions.vendorDeposit(tx['from']).call()
    else:
        return -1
    return _w3.fromWei(result, 'ether')


def registerVendor(_w3, _name, _value=1):
    mgmtContract = ManagementContract(_w3)
    tx = txTmpl
    if _value < mgmtContract.functions.registrationDeposit().call():
        return "Failed. Payment is low."
    elif _value > _w3.eth.getBalance(tx['from']):
        return "Failed. No enough funds to deposit."
    elif mgmtContract.functions.registeredVendorAddress(tx['from']).call():
        return "Failed. The vendor address already used."
    else:
        try:
            tx['value'] = _value
            txHash = mgmtContract.functions.registerVendor(_name.encode()).transact(tx)
            receipt = wait_for_transaction_receipt(_w3, txHash)
            if receipt.status == 1:
                return mgmtContract.events.Vendor().processReceipt(receipt)[0]['args']['tokenId']
        except ValueError:
            return "Failed. The vendor name is not unique."


def regBat(_w3, _count, _value=0):
    _tx = txTmpl
    _tx['value'] = _value
    _batkeys = []
    _args = []
    if deposit(_w3) + web3.fromWei(_value, "ether") < batfee(_w3) * _count:
        return -1
    for i in range(_count):
        _privKey = _w3.sha3(os.urandom(20))
        _batkeys.append((_privKey, _w3.eth.account.privateKeyToAccount(_privKey).address))
    for i in range(len(_batkeys)):
        _args.append(_w3.toBytes(hexstr=_batkeys[i][1]))

    result, _batkeysSuccess = safetySendTx(_w3, _tx, _args)
    form = 'Created battery with ID: {}'
    resultText = ''
    if result != 0:
        for _batId in _batkeys:
            createBatteryScript(_batId)
            print(form.format(_batId[1]))
    return result


def createBatteryScript(_key):
    _path = './battery_template.py'
    with open(_path, 'r') as file:
        code = file.read()
    code = code.replace("cafecafecafecafecafecafecafecafecafecafecafecafecafecafecafecafe",
                        delHexPrefix(web3.toHex(_key[0])), 1)
    with open('./firmware/' + delHexPrefix(_key[1][2:10]) + '.py', 'w') as file:
        file.write(code)
    return True


def newowner(_w3, _batids, _newOwner, pacTransf=False):
    _tx = txTmpl
    mgmtContract = ManagementContract(_w3)
    _batteryContract = mgmtContract.functions.batteryManagement().call()
    _BatContract = BatteryContract(_w3, _batteryContract)
    if pacTransf:
        with open(_batids, 'r') as file:
            _batids = file.read().split('\n')
    else:
        _batids = [_batids]
    _args = []
    for i in range(len(_batids)):
        _args.append(_w3.toBytes(hexstr=_batids[i]))
    if mgmtContract.functions.serviceCenters(_newOwner).call() or mgmtContract.functions.cars(_newOwner).call():
        try:
            txHash = _BatContract.functions.transfer(_newOwner, _args).transact(_tx)
            receipt = wait_for_transaction_receipt(_w3, txHash)
            result = receipt.status
        except:
            result = 0
    else:
        result = 0

    return result == 1


def create_parser():
    parser = argparse.ArgumentParser(
        description='Simple management tool for didgital assests',
        epilog="""
It is expected that Web3 provider specified by WEB3_PROVIDER_URI
environment variable. E.g.
WEB3_PROVIDER_URI=file:///path/to/node/rpc-json/file.ipc
WEB3_PROVIDER_URI=http://192.168.1.2:8545
"""
    )

    parser.add_argument(
        '--regfee', action='store_true', required=False,
        help='Get fee (in ether) for registration request'
    )
    parser.add_argument(
        '--batfee', action='store_true', required=False,
        help='Get fee (in ether) for registration request'
    )

    parser.add_argument(
        '--new', nargs=1, required=False,
        help='Add address to the account.json'
    )

    parser.add_argument(
        '--reg', nargs=2, required=False,
        help='Register a vendor'
    )
    parser.add_argument(
        '--bat', nargs="+", required=False,
        help='Register batteries'
    )

    parser.add_argument(
        '--owner', nargs=2, required=False,
        help='Transfer batteries'
    )

    parser.add_argument(
        '--deposit', action='store_true', required=False,
        help='Get deposit for vendor account'
    )

    return parser


def main():
    parser = create_parser()

    args = parser.parse_args()

    web3.eth.enable_unaudited_features()

    # print(args)
    if args.new:
        global database
        database = {}
        database['account'] = web3.personal.newAccount(args.new[0])
        database['password'] = args.new[0]
        if len(database['account']) == 42:
            writeDatabase('account.json')
            print('{}'.format(database['account']))
        else:
            sys.exit("Cannot get address")
    elif args.regfee:
        if 'mgmtContract' in database:
            unlockAccount(web3)
            result = regfee(web3)
            print('Vendor registration fee: {}'.format(result))
    elif args.batfee:
        if 'mgmtContract' in database:
            unlockAccount(web3)
            result = batfee(web3)
            print('Production fee per one battery: {}'.format(result))
    elif args.reg:
        if 'mgmtContract' in database:
            unlockAccount(web3)
            result = registerVendor(web3, args.reg[0], web3.toWei(args.reg[1], 'ether'))
            if isinstance(result, bytes):
                print('Success.\nVendor ID: {}'.format(delHexPrefix(web3.toHex(result))))
            else:
                sys.exit(result)

        else:
            sys.exit("Management contract address not found in the database")
    elif args.bat:
        if 'mgmtContract' in database:
            unlockAccount(web3)
            if len(args.bat) > 1:
                result = regBat(web3, int(args.bat[0]), web3.toWei(args.bat[1], 'ether'))
            else:
                result = regBat(web3, int(args.bat[0]))
            # if result >= 1:
            #     print('Success')
            # else:
            #     print('Failed')
            if result < 1:
                print('Failed')
        else:
            sys.exit("Management contract address not found in the database")

    elif args.owner:
        if 'mgmtContract' in database:
            unlockAccount(web3)
            isFilePathExist = os.access(args.owner[0], os.F_OK)
            if isFilePathExist:
                result = newowner(web3, args.owner[0], args.owner[1], pacTransf=True)
            else:
                result = newowner(web3, args.owner[0], args.owner[1], pacTransf=False)
            if result:
                print('Success')
            else:
                print('Failed. Not allowed to change ownership.')
    elif args.deposit:
        if 'mgmtContract' in database:
            unlockAccount(web3)
            result = deposit(web3)
            if result != -1:
                print('Deposit: {}'.format(result))
            else:
                print("Vendor account is not registered.")
    else:
        sys.exit("No parameters provided")


if __name__ == '__main__':
    main()

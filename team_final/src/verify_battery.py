from utils import openDataBase, ManagementContract, BatteryContract, DealContract
import json, subprocess, os


def _delHexPrefix(_s: str):
    if _s[:2] == '0x':
        _s = _s[2:]
    return _s

def batteryExists(_path):
    return os.path.exists(_path)


def verify(_w3, _path):
    n, t, v, r, s = return_ntvrs_from(_path)
    mgmtContract = ManagementContract(_w3)
    validity, vendorAddress = _verifyBatteryByContract(_w3, mgmtContract, n, t, v, r, s)

    vendorId = _w3.toHex(mgmtContract.functions.vendorId(vendorAddress).call())
    vendorName = mgmtContract.functions.vendorNames(vendorId).call()
    if validity == 0:
        return validity, n, _delHexPrefix(vendorId), vendorName.decode()
    else:
        return [validity]


def return_ntvrs_from(_path):
    args = ['python', _path, '--getJson']

    batteryOut = subprocess.Popen(args, stdout=subprocess.PIPE)
    batteryInformation = json.loads(batteryOut.communicate()[0].decode())
    n, t, v, r, s = batteryInformation['n'], batteryInformation['t'], batteryInformation['v'], \
                    batteryInformation['r'], batteryInformation['s']
    return [n, t, v, r, s]


def _verifyBatteryByContract(_w3, _mgmtContract, _n, _t, _v, _r, _s):
    batteryContractAddress = _mgmtContract.functions.batteryManagement().call()

    batteryContract = BatteryContract(_w3, batteryContractAddress)

    validity, address = batteryContract.functions.verifyBattery(_n, _t, _v, _w3.toBytes(hexstr=_r),
                                                                _w3.toBytes(hexstr=_s)).call()

    return validity, address

import subprocess, json, os, sys

accounts = {}
contracts = {}
batteries = []

password = 'hackathon180317'
setup = 'setup.py'
vendor = 'vendor.py'
car = 'car.py'
scenter = 'scenter.py'
fauc = 'testnet/faucet.py'

py = ['python']

def faucet(_account):
    print()
    args = py + [fauc, _account]
    print(*args)
    subprocess.Popen(args, stdout=subprocess.PIPE).communicate()

def createManagementAccount():
    global accounts
    args = py + [setup, '--new', password]
    print(*args)
    out = subprocess.Popen(args, stdout=subprocess.PIPE).communicate()
    carry = out[0].decode().splitlines()
    accounts['managementAccount'] = carry[0]
    faucet(carry[0])

def setupSystem(_fee):
    global contracts
    print()
    args = py + [setup, '--setup',  _fee]
    print(*args)
    out = subprocess.Popen(args, stdout=subprocess.PIPE).communicate()
    carry = out[0].decode().splitlines()
    contracts['mgmtContractAddress'] = carry[0].split(': ')[1]
    contracts['serviceProviderWalletAddress'] = carry[1].split(': ')[1]
    contracts['currencyTokenContractAddress'] = carry[2].split(': ')[1]

def createVendorAccount():
    print()
    args = py + [vendor, '--new', password]
    print(*args)
    out = subprocess.Popen(args, stdout=subprocess.PIPE).communicate()
    carry = out[0].decode().splitlines()
    accounts['vendorAccount'] = carry[0]
    faucet(carry[0])

def registerNewVendor():
    global accounts
    print()
    args = py + [vendor, '--reg', 'InnoCars', '0.1']
    print(*args)
    out = subprocess.Popen(args, stdout=subprocess.PIPE).communicate()
    accounts['vendorId'] = out[0].decode().splitlines()[1].split(': ')[1]

def createNewBatteries(_batNunber):
    global batteries
    print()
    args = py + [vendor, '--bat', _batNunber]
    print(*args)
    out = subprocess.Popen(args, stdout=subprocess.PIPE).communicate()
    out = out[0].decode().splitlines()
    for i in range(len(out)):
        batteries.append(out[i].split(': ')[1])

def createNewScenterAccount():
    global accounts
    print()
    args = py + [scenter, '--new', password]
    print(*args)
    out = subprocess.Popen(args, stdout=subprocess.PIPE).communicate()
    out = out[0].decode().splitlines()
    faucet(out[0])
    accounts['scenterAccount'] = out[0]

def registerScenterAccount():
    print()
    args = py + [scenter, '--reg']
    print(*args)
    subprocess.Popen(args, stdout=subprocess.PIPE).communicate()

def createNewCarAccount():
    global accounts
    print()
    args = py + [car, '--new']
    print(*args)
    out = subprocess.Popen(args, stdout=subprocess.PIPE).communicate()
    out = out[0].decode().splitlines()
    faucet(out[0])
    accounts['carAccount'] = out[0]

def registerCarAccount():
    print()
    args = py + [car, '--reg']
    print(*args)
    subprocess.Popen(args, stdout=subprocess.PIPE).communicate()

def saleNewBattery(_batteryId, _newOwner):
    print()
    args = py + [vendor, '--owner', _batteryId, _newOwner]
    print(*args)
    out = subprocess.Popen(args, stdout=subprocess.PIPE).communicate()
    print(out[0].decode())

def createContract(_newBattery, _oldBattery, _serviceFee):
    print()
    args = py + [scenter, '--contract', f'firmware/{_newBattery[2:10]}.py', f'firmware/{_oldBattery[2:10]}.py', accounts['carAccount'], _serviceFee]
    print(*args)
    out = subprocess.Popen(args, stdout=subprocess.PIPE).communicate()
    print(out[0].decode())

def main():
    createManagementAccount()
    setupSystem('0.0001')
    createVendorAccount()
    registerNewVendor()
    createNewBatteries('5')
    createNewScenterAccount()
    registerScenterAccount()
    createNewCarAccount()
    registerCarAccount()
    saleNewBattery(batteries[0], accounts['scenterAccount'])
    saleNewBattery(batteries[1], accounts['carAccount'])
    createContract(batteries[0], batteries[1], '1')

    print()
    for i in list(contracts.keys()):
        print(i + ': ' + contracts[i])
    print()
    for i in list(accounts.keys()):
        print(i + ': ' + accounts[i])
    print()
    print("Batteries:")
    for i in batteries:
        print(i)

if __name__ == '__main__':
    # contracts = {'mgmtContractAddress': '0x50835cc7c28Da19878415758e6F562D46c324F4F',
    #             'serviceProviderWalletAddress': '0x2c5b6db368F2df6D5a6902f45C9A006f46DF7bA1',
    #             'currencyTokenContractAddress': '0x1c741C179aD7611658b6cd7f9296ef301578FebF'}
    #
    # accounts = {'managementAccount': '0x806FA8996d43316D8be233BEF54e6b1CeDb9Ed79',
    #              'vendorAccount': '0x7d2B0DcF57209bc8495F2a9b1bE41E4A9864B40A',
    #              'vendorId': '875e6b38',
    #              'scenterAccount': '0xdB4df1cA65967b5455FD6B3eDE01fcdEA563F3f6',
    #              'carAccount': '0x433Eac9aE288ac86eF548512bC2db29Be41BBf1B'}
    #
    # batteries = ['0xaA3AbBee1718bFb14407e7e848469F5ea3FFbE14',
    #              '0xF84e7ddcD1bE0452d3B39fab0a2B1b6c23a98dBC',
    #              '0xeC891e1Ce6fb92b33e87a7d2e9961F3B83711a96',
    #              '0xa86d1Ce0Cb1B5A4be27Ee44d9e3a335bf542e634',
    #              '0xd4f605B79874851A57E34f165Aa636F089bF0cE6']

    main()
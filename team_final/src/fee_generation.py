minimalFeeValue = 1.003
maximalFeeValue = 1.0035

def generateMinFee():
    min_fee = []
    for delta in range(101, 300):
        min_fee.append(int(delta * (maximalFeeValue ** delta)))
    return min_fee

def generateMaxFee():
    max_fee = []
    for delta in range(1, 250):
        max_fee.append(int(delta * (minimalFeeValue ** delta)))
    return max_fee
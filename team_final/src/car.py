import warnings

warnings.simplefilter("ignore", category=DeprecationWarning)

# install web3 >=4.0.0 by `pip install --upgrade 'web3==4.0.0b11'`
from web3 import Web3
from web3.middleware import geth_poa_middleware
from web3.utils.transactions import wait_for_transaction_receipt

import datetime as dt
from random import randint

import argparse
import sys, os

from utils import writeDataBase, openDataBase, getActualGasPrice, compileContracts, \
    initializeContractFactory, runCmd, ManagementContract, BatteryContract, DealContract

from verify_battery import verify, return_ntvrs_from, batteryExists

web3 = Web3()

# configure provider to work with PoA chains
web3.middleware_stack.inject(geth_poa_middleware, layer=0)

accountDatabaseName = 'car.json'
mgmtContractDatabaseName = 'database.json'

gasPrice = getActualGasPrice(web3)

registrationRequiredGas = 50000


def _createCarAccountDatabase(_carPrivateKey):
    data = {'key': _carPrivateKey}
    writeDataBase(data, accountDatabaseName)


# generate private key using current time and random int
def _generatePrivateKey():
    t = int(dt.datetime.utcnow().timestamp())
    k = randint(0, 2 ** 16)
    privateKey = web3.toHex(web3.sha3((t + k).to_bytes(32, 'big')))
    return _delHexPrefix(privateKey)


# delete hex prefix from string
def _delHexPrefix(_s: str):
    if _s[:2] == '0x':
        _s = _s[2:]
    return _s


def new():
    privateKey = _generatePrivateKey()
    data = {"key": privateKey}
    writeDataBase(data, accountDatabaseName)
    print(web3.eth.account.privateKeyToAccount(data['key']).address)


def account():
    data = openDataBase(accountDatabaseName)
    if data is None:
        print("Cannot access account database")
        return

    privKey = data['key']
    print(web3.eth.account.privateKeyToAccount(privKey).address)


def reg():
    mgmtContract = ManagementContract(web3)

    data = openDataBase(accountDatabaseName)
    if data is None:
        print("Cannot access account database")
        return

    privateKey = data['key']
    carAddress = web3.eth.account.privateKeyToAccount(privateKey).address

    if registrationRequiredGas * gasPrice > web3.eth.getBalance(carAddress):
        print("No enough funds to send transaction")
        return

    nonce = web3.eth.getTransactionCount(carAddress)
    tx = {'gasPrice': gasPrice, 'nonce': nonce}

    regTx = mgmtContract.functions.registerCar().buildTransaction(tx)
    signTx = web3.eth.account.signTransaction(regTx, privateKey)
    txHash = web3.eth.sendRawTransaction(signTx.rawTransaction)
    receipt = wait_for_transaction_receipt(web3, txHash)

    if receipt.status == 1:
        print('Registered successfully')
    else:
        print('Already registered')


CarExe = 'car.py'


def AgreeToDeal(_actor, _path, _addressDeal):
    retval = False
    if not os.path.exists(_path):
        print('Not found path ')
        return retval

    mgmtContract = ManagementContract(web3)
    _addressBat = mgmtContract.functions.batteryManagement.call()
    BatContract = BatteryContract(web3, _addressBat)
    dealCont = DealContract(web3, _addressDeal)
    if not batteryExists(_path):
        print('Cannot access battery')
        return
    _n, _t, _v, _r, _s = return_ntvrs_from(_path)
    _tokenId = BatContract.functions.getBatteryIdFromSignature(_n, _t, _v, _r, _s)
    txHash1 = BatContract.functions.delegatedApprove(_addressDeal, _tokenId, _v, _r, _s)
    _p = _n * (2 ** 64) + _t * (2 ** 32) + _v
    txHash2 = dealCont.functions.agreeToDeal(_p, _r, _s)
    receipt1 = wait_for_transaction_receipt(web3, txHash1)
    receipt2 = wait_for_transaction_receipt(web3, txHash2)
    if receipt1.status == 1:
        if receipt2.status == 1:
            retval = True
        else:
            print("AgreeToDeal from Deal contract did not work")
    else:
        print("getBatteryIdFromSignature from Battery contract did not work")
    return retval


def GetInfoAboutContract(_actor, _addressDeal):
    dealCont = DealContract(web3, _addressDeal)
    forma = """Total charges: {}\nVendor ID: {}\nVendor Name: '{}'"""
    try:
        old_bat_info = dealCont.functions.oldBatteryInfo().call()
        new_bat_info = dealCont.functions.oldBatteryInfo().call()
    except BaseException as error:
        return False
    print("OLD BATTERY:")
    print(forma.format(old_bat_info))
    print("NEW BATTERY:")
    print(forma.format(new_bat_info))
    return True


def create_parser():
    parser = argparse.ArgumentParser(
        description='Service provider tool',
        epilog="""
    It is expected that Web3 provider specified by WEB3_PROVIDER_URI
    environment variable. E.g.
    WEB3_PROVIDER_URI=file:///path/to/node/rpc-json/file.ipc
    WEB3_PROVIDER_URI=http://192.168.1.2:8545
    """
    )

    parser.add_argument(
        '--new', action='store_true', required=False,
        help='Add new car account'
    )

    parser.add_argument(
        '--account', action='store_true', required=False,
        help='Return address of car'
    )

    parser.add_argument(
        '--reg', action='store_true', required=False,
        help='Register car in the chain'
    )

    parser.add_argument(
        '--verify', type=str, required=False,
        help='Verify battery on validity'
    )

    parser.add_argument(
        '--deal', nargs=2, required=False,
        help='Confirm deal contract'
    )
    parser.add_argument(
        '--info', type=str, required=False,
        help='Show information about contract for change batteries'
    )
    return parser


def main():
    parser = create_parser()
    args = parser.parse_args()

    web3.eth.enable_unaudited_features()

    if args.new:
        new()
    elif args.account:
        account()
    elif args.reg:
        reg()
    elif args.verify:
        if batteryExists(args.verify):
            result = verify(web3, args.verify)
            if result[0] == 0:
                n = result[1]
                vendorId = result[2]
                vendorName = result[3]
                print("Verified successfully.")
                print('Total charges: {}'.format(n))
                print('Vendor ID: {}'.format(vendorId))
                print('Vendor Name: {}'.format(vendorName))
            elif result[0] == 1:
                print('Battery with the same status already replaced. Probably the battery forged.')
            elif result[0] == 2:
                print('Verifiсation failed. Probably the battery forged.')
            else:
                print('Error. Cannot verify battery.')
        else:
            print('Cannot access battery')
    elif args.info:
        pass
    else:
        sys.exit("No parameters provided")


if __name__ == '__main__':
    main()
